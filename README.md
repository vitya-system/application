# Vitya PHP application framework

This package provides a small framework for applications, based on Vitya
[Vitya application framework components](https://gitlab.com/vitya-system/components)

The easiest way to start with the framework is to install
[the basic application template](https://gitlab.com/vitya-system/application-template).

Need a full-fledged CMS? Then have a look at
[the CMS application template](https://gitlab.com/vitya-system/cms-template).

Vitya aims to provide a simple CMS that can be easily extended. We try to keep
the code small and easy to understand.
