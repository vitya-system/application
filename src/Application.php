<?php

/*
 * Copyright 2020, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application;

use Exception;
use Vitya\Application\ListenerProvider\AuthenticationExceptionListenerProvider;
use Vitya\Application\ListenerProvider\ControllerExceptionListenerProvider;
use Vitya\Application\Middleware\DebugMiddleware;
use Vitya\Application\Middleware\HttpsMiddleware;
use Vitya\Application\Middleware\SessionMiddleware;
use Vitya\Application\ServiceProvider\AssetManagerServiceProvider;
use Vitya\Application\ServiceProvider\CacheMachineServiceProvider;
use Vitya\Application\ServiceProvider\CacheServiceProvider;
use Vitya\Application\ServiceProvider\CliFrontendServiceProvider;
use Vitya\Application\ServiceProvider\DebugInfoCollectorServiceProvider;
use Vitya\Application\ServiceProvider\DependencyInjectorServiceProvider;
use Vitya\Application\ServiceProvider\EventDispatcherServiceProvider;
use Vitya\Application\ServiceProvider\HttpFactoryServiceProvider;
use Vitya\Application\ServiceProvider\LocalFileStorageServiceProvider;
use Vitya\Application\ServiceProvider\LogServiceProvider;
use Vitya\Application\ServiceProvider\MachineServiceProvider;
use Vitya\Application\ServiceProvider\RequestHandlerServiceProvider;
use Vitya\Application\ServiceProvider\RouteCollectionServiceProvider;
use Vitya\Application\ServiceProvider\RouterServiceProvider;
use Vitya\Application\ServiceProvider\TemporaryStorageServiceProvider;
use Vitya\Application\ServiceProvider\WebFrontendServiceProvider;
use Vitya\Component\Service\ServiceContainer;

class Application extends ServiceContainer
{
    private $extensions = [];

    public function __construct(string $app_dir)
    {
        parent::__construct();
        $this['app_dir'] = $app_dir;
        $this['config'] = [
            'application_name' => 'Default application name',
            'application_homepage_url' => '/',
            'base_url_path' => '',
            'cache_dir' => realpath($app_dir . '/../cache'),
            'cli_dir' => realpath($app_dir . '/../../cli'),
            'data_dir' => realpath($app_dir . '/../data'),
            'log_dir' => realpath($app_dir . '/../log'),
            'sessions_dir' => realpath($app_dir . '/../sessions'),
            'tmp_dir' => realpath($app_dir . '/../tmp'),
            'web_dir' => realpath($app_dir . '/../../web'),
            'debug' => false,
            'default_timezone' => 'UTC',
            'enable_default_cache_machine_service' => true,
            'environment' => 'dev',
        ];
        $this['config'] = [
            'assets' => [],
            'cache_machine__allow_client_to_bypass_cache' => false,
            'db' => [],
            'https__force_https' => false,
            'https__secure_cookies' => true,
            'https__hsts_max_age' => 31536000,
            'https__hsts_include_subdomains' => false,
            'image__backend' => 'gd2',
            'l10n__locales' => [
                'en' => [
                    'name' => 'English',
                ],
            ],
            'l10n__default_locale' => 'en',
            'log' => [],
            'log__default_logger' => 'app',
            'session__name' => 'vitya-session-id',
            'session__gc_probability' => 0.01,
            'session__lifetime' => 1800,
            'mailer__dsn' => 'sendmail://default',
            'web_frontend__forced_domain_name' => '',
        ] + $this['config'];
    }

    public function __toString(): string
    {
        return 'Vitya Application';
    }

    public function getExtensions(): array
    {
        return $this->extensions;
    }

    public function getExtension(string $class_name): ApplicationExtensionInterface
    {
        if (false === $this->hasExtension($class_name)) {
            throw new Exception('Invalid extension class name: ' . $class_name . '.');
        }
        return $this->extensions[$class_name];
    }

    public function addExtension(ApplicationExtensionInterface $extension): self
    {
        $this->extensions[get_class($extension)] = $extension;
        return $this;
    }

    public function hasExtension(string $class_name): bool
    {
        return isset($this->extensions[$class_name]);
    }

    public function mergeConfig(array $config): self
    {
        $this['config'] = isset($this['config']) ? array_merge($this['config'], $config) : $config;
        return $this;
    }

    public function loadConfigFromJsonFile(string $file): self
    {
        $config = json_decode(file_get_contents($file), true, 16, JSON_THROW_ON_ERROR);
        $this->mergeConfig($config);
        return $this;
    }

    public function mergeRoutes(array $routes): self
    {
        $this['routes'] = isset($this['routes']) ? array_merge($this['routes'], $routes) : $routes;
        return $this;
    }

    public function loadRoutesFromJsonFile(string $file, string $url_path_prefix = ''): self
    {
        $routes = json_decode(file_get_contents($file), true, 16, JSON_THROW_ON_ERROR);
        if ($url_path_prefix !== '') {
            foreach ($routes as $k => $route) {
                if (isset($route['path']) && is_string($route['path'])) {
                    $routes[$k]['path'] = $url_path_prefix . $route['path'];
                }
            }
        }
        $this->mergeRoutes($routes);
        return $this;
    }

    public function parametrize(): self
    {
        $this['application_name'] = (string) $this['config']['application_name'];
        $this['application_homepage_url'] = (string) $this['config']['application_homepage_url'];
        $this['base_url_path'] = (string) $this['config']['base_url_path'];
        // Define application directories.
        $this['cache_dir'] = (string) $this['config']['cache_dir'];
        $this['cli_dir'] = (string) $this['config']['cli_dir'];
        $this['data_dir'] = (string) $this['config']['data_dir'];
        $this['log_dir'] = (string) $this['config']['log_dir'];
        $this['sessions_dir'] = (string) $this['config']['sessions_dir'];
        $this['tmp_dir'] = (string) $this['config']['tmp_dir'];
        $this['web_dir'] = (string) $this['config']['web_dir'];
        // Misc parameters.
        $this['default_timezone'] = (string) $this['config']['default_timezone'];
        $this['debug'] = (bool) $this['config']['debug'];
        $this['enable_default_cache_machine_service'] = (bool) $this['config']['enable_default_cache_machine_service'];
        $this['environment'] = (string) $this['config']['environment'];
        // Asset manager service provider.
        $assets_parameters = [];
        if (is_array($this['config']['assets'])) {
            foreach ($this['config']['assets'] as $asset_pool_name => $asset_pool_definition) {
                $sanitized_asset_pool_definition = [
                    'path' => __DIR__,
                    'etag' => '',
                    'etag_file' => '',
                    'max_age' => 0,
                    'gzip' => [],
                ];
                if (isset($asset_pool_definition['path']) && is_string($asset_pool_definition['path'])) {
                    $path = $asset_pool_definition['path'];
                    $path = str_replace('{app_dir}', $this['app_dir'], $path);
                    $sanitized_asset_pool_definition['path'] = $path;
                }
                if (isset($asset_pool_definition['etag']) && is_string($asset_pool_definition['etag'])) {
                    $sanitized_asset_pool_definition['etag'] = $asset_pool_definition['etag'];
                }
                if (isset($asset_pool_definition['etag_file']) && is_string($asset_pool_definition['etag_file'])) {
                    $etag_file = $asset_pool_definition['etag_file'];
                    $etag_file = str_replace('{app_dir}', $this['app_dir'], $etag_file);
                    $sanitized_asset_pool_definition['etag_file'] = $etag_file;
                }
                if (isset($asset_pool_definition['max_age']) && is_int($asset_pool_definition['max_age'])) {
                    $sanitized_asset_pool_definition['max_age'] = $asset_pool_definition['max_age'];
                }
                if (isset($asset_pool_definition['gzip']) && is_array($asset_pool_definition['gzip'])) {
                    $sanitized_asset_pool_definition['gzip'] = $asset_pool_definition['gzip'];
                }
                $assets_parameters[$asset_pool_name] = $sanitized_asset_pool_definition;
            }
        }
        $this['assets'] = $assets_parameters;
        // Cache machine service provider.
        $this['cache_machine__allow_client_to_bypass_cache'] = (bool) $this['config']['cache_machine__allow_client_to_bypass_cache'];
        // Db service provider.
        $db_parameters = [];
        if (is_array($this['config']['db'])) {
            foreach ($this['config']['db'] as $db_key => $db_config) {
                if (isset($db_config['url'])) {
                    $url = (string) $db_config['url'];
                    $url = str_replace('{data_dir}', $this['data_dir'], $url);
                    $db_parameters[$db_key] = [
                        'url' => $url,
                    ];
                }
            }
        }
        $this['db'] = $db_parameters;
        // Https service parameters.
        $this['https__force_https'] = (bool) $this['config']['https__force_https'];
        $this['https__secure_cookies'] = (bool) $this['config']['https__secure_cookies'];
        $this['https__hsts_max_age'] = (int) $this['config']['https__hsts_max_age'];
        $this['https__hsts_include_subdomains'] = (bool) $this['config']['https__hsts_include_subdomains'];
        // Image backend.
        $this['image__backend'] = (string) $this['config']['image__backend'];
        // L10n service parameters.
        if (false === is_array($this['config']['l10n__locales'])) {
            throw new Exception('Invalid l10n__locales configuration.');
        }
        $locale_parameters = [];
        foreach ($this['config']['l10n__locales'] as $locale_id => $locale_description) {
            if (false === isset($locale_description['name']) || false === is_string($locale_description['name'])) {
                throw new Exception('A name must be provided as a string for locale ' . $locale_id . '.');
            }
            $locale_parameters[$locale_id] = [
                'name' => $locale_description['name'],
            ];
        }
        $this['l10n__locales'] = $locale_parameters;
        if (isset($this['config']['l10n__default_locale']) && isset($this['l10n__locales'][$this['config']['l10n__default_locale']])) {
            $this['l10n__default_locale'] = $this['config']['l10n__default_locale'];
        } else {
            $this['l10n__default_locale'] = array_key_first($this['l10n__locales']);
        }
        // Log service provider.
        $log_parameters = [];
        if (is_array($this['config']['log'])) {
            foreach ($this['config']['log'] as $log_key => $log_config) {
                $sanitized_log_config = [
                    'type' => '',
                    'options' => [],
                ];
                if (isset($log_config['type']) && is_string($log_config['type'])) {
                    $sanitized_log_config['type'] = $log_config['type'];
                }
                if (isset($log_config['options']) && is_array($log_config['options'])) {
                    $sanitized_log_config['options'] = $log_config['options'];
                }
                $log_parameters[$log_key] = $sanitized_log_config;
            }
        }
        $this['log'] = $log_parameters;
        if (false === isset($this['log']['app'])) {
            $this['log'] = [
                'app' => [
                    'type' => 'basic',
                    'options' => [
                        'path' => '{log_dir}/app.log',
                        'rotate' => true,
                        'debug' => $this['debug'],
                    ],
                ],
            ] + $this['log'];
        }
        if (false === isset($this['log']['auth'])) {
            $this['log'] = [
                'auth' => [
                    'type' => 'basic',
                    'options' => [
                        'path' => '{log_dir}/auth.log',
                        'rotate' => true,
                        'debug' => $this['debug'],
                    ],
                ],
            ] + $this['log'];
        }
        $this['log__default_logger'] = (string) $this['config']['log__default_logger'];
        // Router service parameters.
        $this->loadRoutesFromJsonFile(realpath(__DIR__ . '/../routes.json'));
        // Session service parameters.
        $this['session__name'] = (string) $this['config']['session__name'];
        $this['session__gc_probability'] = (float) $this['config']['session__gc_probability'];
        $this['session__lifetime'] = (int) $this['config']['session__lifetime'];
        // Mailer service parameters.
        $this['mailer__dsn'] = (string) $this['config']['mailer__dsn'];
        // Web frontend service parameters.
        $this['web_frontend__forced_domain_name'] = (string) $this['config']['web_frontend__forced_domain_name'];
        // Register services.
        $this->register(new AssetManagerServiceProvider());
        $this->register(new CliFrontendServiceProvider());
        $this->register(new DependencyInjectorServiceProvider());
        if (false !== $this['debug']) {
            $this->register(new DebugInfoCollectorServiceProvider());
        }
        $this->register(new EventDispatcherServiceProvider());
        $this->register(new HttpFactoryServiceProvider());
        $this->register(new LocalFileStorageServiceProvider());
        $this->register(new LogServiceProvider());
        $this->register(new MachineServiceProvider());
        $this->register(new RequestHandlerServiceProvider());
        $this->register(new RouteCollectionServiceProvider());
        $this->register(new RouterServiceProvider());
        $this->register(new TemporaryStorageServiceProvider());
        $this->register(new WebFrontendServiceProvider());
        if (false !== $this['enable_default_cache_machine_service']) {
            if (false === $this->has('cache')) {
                $this->register(new CacheServiceProvider());
            }
            $this->extend(new CacheMachineServiceProvider());
        }
        foreach ($this->extensions as $extension) {
            $extension->parametrize();
        }
        return $this;
    }

    public function init(): self
    {
        // Misc.
        if ('' !== $this['default_timezone']) {
            date_default_timezone_set($this['default_timezone']);
        }
        // Middleware.
        if ($this->has('session')) {
            $secure_session = $this['https__force_https'] && $this['https__secure_cookies'];
            $this->get('request_handler')->addMiddleware(
                new SessionMiddleware($this->get('session'), $this->get('http_factory'), $secure_session, $this['base_url_path']),
                512
            );
        }
        $this->get('request_handler')->addMiddleware(
            new HttpsMiddleware(
                $this->get('http_factory'),
                $this['https__force_https'],
                $this['https__secure_cookies'],
                $this['https__hsts_max_age'],
                $this['https__hsts_include_subdomains'],
            ),
            0
        );
        if (false !== $this['debug']) {
            if (PHP_VERSION_ID >= 70400) {
                ini_set('zend.exception_ignore_args', '0');
            }
            $this->get('request_handler')->addMiddleware(
                new DebugMiddleware($this->get('debug_info_collector'), $this->get('http_factory')),
                -2048
            );
        }
        // Event listeners.
        $this->get('event_dispatcher')->addListenerProvider(
            new AuthenticationExceptionListenerProvider(),
            256
        );
        $this->get('event_dispatcher')->addListenerProvider(
            new ControllerExceptionListenerProvider($this->get('http_factory'), $this->get('http_factory'), $this->get('log'), $this['debug']),
            0
        );
        // Container parameters injection.
        $this->get('dependency_injector')->bindParameter('Vitya\Application\Controller\AuthenticationController::processLogout', 'base_url_path', 'base_url_path');
        foreach ($this->extensions as $extension) {
            $extension->init();
        }
        return $this;
    }

}
