<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vitya\Component\Http\CookieFactoryInterface;
use Vitya\Component\Session\SessionService;

class SessionMiddleware implements MiddlewareInterface
{
    private $basePath = '';
    private $cookieFactory = null;
    private $secure = false;
    private $sessionService = null;

    public function __construct(SessionService $session_service, CookieFactoryInterface $cookie_factory, bool $secure, string $base_path = '')
    {
        $this->basePath = $base_path;
        $this->cookieFactory = $cookie_factory;
        $this->secure = $secure;
        $this->sessionService = $session_service;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->secure) {
            if ($this->basePath === '') {
                $cookie_name = '__Host-' . $this->sessionService->getSessionName();
            } else {
                $cookie_name = '__Secure-' . $this->sessionService->getSessionName();
            }
        } else {
            $cookie_name = $this->sessionService->getSessionName();
        }
        $cookie_params = $request->getCookieParams();
        if (isset($cookie_params[$cookie_name])) {
            $this->sessionService->setSessionId($cookie_params[$cookie_name]);
        }
        $response = $handler->handle($request);
        $session_id = $this->sessionService->getSessionId();
        if ($session_id !== null && $this->sessionService->wasStarted()) {
            $this->sessionService->end();
            $cookie = $this->cookieFactory
                ->createCookie(
                    $cookie_name,
                    $session_id,
                    null)
                ->withPath($this->basePath . '/')
                ->withHttpOnly(true)
                ->withSameSite('Strict');
            $response = $response
                ->withAddedHeader('Set-Cookie', (string) $cookie)
                ->withHeader('Cache-Control', 'private, must-revalidate, max-age=0');
        }
        return $response;
    }

}
