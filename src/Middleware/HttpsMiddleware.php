<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Middleware;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HttpsMiddleware implements MiddlewareInterface
{
    private $forceHttps = false;
    private $responseFactory = null;
    private $secureCookies = false;
    private $hstsMaxAge = 0;
    private $hstsIncludeSubdomains = false;

    public function __construct(
        ResponseFactoryInterface $response_factory,
        bool $force_https,
        bool $secure_cookies,
        int $hsts_max_age,
        bool $hsts_include_subdomains
    ) {
        $this->forceHttps = $force_https;
        $this->responseFactory = $response_factory;
        $this->secureCookies = $secure_cookies;
        $this->hstsMaxAge = $hsts_max_age;
        $this->hstsIncludeSubdomains = $hsts_include_subdomains;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->forceHttps && $request->getUri()->getScheme() !== 'https') {
            $response = $this->responseFactory
                ->createResponse(308)
                ->withHeader('Location', (string) $request->getUri()->withScheme('https')->withPort(null))
            ;
            return $response;
        }
        $response = $handler->handle($request);
        if ($this->forceHttps && $this->hstsMaxAge > 0) {
            $hsts_header_content = 'max-age=' . $this->hstsMaxAge;
            if ($this->hstsIncludeSubdomains) {
                $hsts_header_content .= '; includeSubDomains';
            }
            $response = $response->withHeader('Strict-Transport-Security', $hsts_header_content);
        }
        if ($this->forceHttps && $this->secureCookies) {
            $set_cookie_lines = [];
            foreach ($response->getHeader('Set-Cookie') as $response_header) {
                $directives = array_map('trim', explode(';', $response_header));
                if (!empty($directives)) {
                    if (!in_array('Secure', $directives)) {
                        $directives[] = 'Secure';
                    }
                    $set_cookie_lines[] = implode('; ', $directives);
                }
            }
            $response = $response->withoutHeader('Set-Cookie');
            foreach ($set_cookie_lines as $set_cookie_line) {
                $response = $response->withAddedHeader('Set-Cookie', $set_cookie_line);
            }
        }
        return $response;
    }

}
