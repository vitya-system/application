<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vitya\Component\Debug\DebugInfoCollector;

class DebugMiddleware implements MiddlewareInterface
{
    private $debugInfoCollector = null;
    private $streamFactory = null;

    public function __construct(DebugInfoCollector $debug_info_collector, StreamFactoryInterface $stream_factory)
    {
        $this->debugInfoCollector = $debug_info_collector;
        $this->streamFactory = $stream_factory;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $content_type = mb_strtolower($response->getHeaderLine('Content-Type'));
        if ($content_type === 'text/html; charset=utf-8') {
            if ($response->getBody()->isSeekable()) {
                $response->getBody()->rewind();
                $s = $response->getBody()->getContents();
                $debug_elements = [];
                // Memory usage.
                $memory_peak_usage = memory_get_peak_usage();
                $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
                $factor = floor((strlen((string) $memory_peak_usage) - 1) / 3);
                $formatted_memory_peak_usage = sprintf("%.1f", $memory_peak_usage / pow(1024, $factor)) . ' ' . $size[$factor];
                $memory_usage_debug_info = [
                    'label' => 'Memory usage',
                    'value' => [
                        [
                            'label' => 'Human-readable format',
                            'value' => $formatted_memory_peak_usage,
                        ],
                        [
                            'label' => 'Raw format',
                            'value' => $memory_peak_usage,
                        ],
                    ],
                ];
                $this->debugInfoCollector->add($memory_usage_debug_info);
                // Execution time.
                $server_params = $request->getServerParams();
                if (isset($server_params['REQUEST_TIME_FLOAT'])) {
                    $execution_time = microtime(true) - $server_params['REQUEST_TIME_FLOAT'];
                    $execution_time_debug_info = [
                        'label' => 'Execution time (in seconds)',
                        'value' => $execution_time,
                    ];
                    $this->debugInfoCollector->add($execution_time_debug_info);
                }
                // Included files.
                $included_files = get_included_files();
                $included_files_debug_info = [
                    'label' => 'Included files',
                    'value' => [
                        [
                            'label' => 'Count',
                            'value' => count($included_files),
                        ],
                        [
                            'label' => 'List',
                            'value' => implode("\n", $included_files),
                        ]
                    ],
                ];
                $this->debugInfoCollector->add($included_files_debug_info);
                // Insert the debug bar code.
                $s = str_ireplace(
                    '</body>',
                    '<script type="application/ld+json">' . json_encode($this->debugInfoCollector->getCollection(), JSON_PRETTY_PRINT) . '</script></body>',
                    $s
                );
                return $response->withBody($this->streamFactory->createStream($s));
            }
        }
        return $response;
    }

}
