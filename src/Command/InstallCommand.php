<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Command;

use Vitya\Application\Application;
use Vitya\Component\Command\CommandInterface;
use Vitya\Component\Command\Terminal;

class InstallCommand implements CommandInterface
{
    private $app = null;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getName(): string
    {
        return 'install';
    }

    public function execute(Terminal $io): int
    {
        foreach ($this->app->getExtensions() as $extension) {
            $io->text('Installing ' . (string) $extension . '... ');
            $extension->install();
            $io->success('Done.');
            $io->nl();
        }
        return 0;
    }

}
