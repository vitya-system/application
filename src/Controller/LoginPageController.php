<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Controller;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriFactoryInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\LoginPageAuthenticator;
use Vitya\Component\Authentication\UserWithUsernameProviderInterface;
use Vitya\Component\Authentication\UserWithUsernameAndPasswordInterface;
use Vitya\Component\Route\RouterInterface;
use Vitya\Component\Session\SessionService;
use Vitya\Component\Log\LoggerCollectionInterface;

class LoginPageController extends AbstractController
{
    public function processLoginPage(
        AuthenticationService $authn,
        UriFactoryInterface $uri_factory,
        RouterInterface $router,
        SessionService $session_service,
        LoggerCollectionInterface $logger_collection
    ): ResponseInterface
    {
        $logger = $logger_collection->getLogger('auth');
        $authenticated_user = null;
        $realm = $this->getMainRequestBodyParam('realm', '');
        $username = $this->getMainRequestBodyParam('username', '');
        $password = $this->getMainRequestBodyParam('password', '');
        $user_provider = $authn->getUserProvider($realm);
        if (false === $user_provider instanceof UserWithUsernameProviderInterface) {
            throw new Exception('User provider must implement UserWithUsernameProviderInterface.');
        }
        if ('' !== $username) {
            $user = $user_provider->loadUserByUsername($username);
            if ($user instanceof UserWithUsernameAndPasswordInterface) {
                if ($user->checkPassword($password)) {
                    $authenticated_user = $user;
                    $logger->notice('User logged in.', $this->getLogContext($realm, $user->getUsername(), $this->getMainRequest()));
                } else {
                    $logger->info('Login attempt failed: invalid password.', $this->getLogContext($realm, $username, $this->getMainRequest()));
                }
            } else {
                $logger->info('Login attempt failed: invalid username.', $this->getLogContext($realm, $username, $this->getMainRequest()));
            }
        }
        if (null === $authenticated_user) {
            $session_service->getSession()->set('authentication__login_page_' . $realm . '_last_error', LoginPageAuthenticator::ERROR_INVALID_USERNAME_OR_PASSWORD);
            $session_service->getSession()->set('authentication__login_page_' . $realm . '_last_username', $username);
            $session_service->getSession()->set('authentication__login_page_' . $realm . '_authenticated_user_identifier', null);
            $login_page_uri = $router->createUri('login-page-' . $realm);
            $this->seeOther($login_page_uri);
        }
        $origin_url = $session_service->getSession()->get('authentication__login_page_' . $realm . '_origin_url', '/');
        $origin_uri = $uri_factory->createUri($origin_url);
        $session_service->getSession()->set('authentication__login_page_' . $realm . '_last_error', LoginPageAuthenticator::OK);
        $session_service->getSession()->set('authentication__login_page_' . $realm . '_last_username', '');
        $session_service->getSession()->set('authentication__login_page_' . $realm . '_authenticated_user_identifier', $authenticated_user->getUserIdentifier());
        $session_service->getSession()->set('authentication__login_page_' . $realm . '_origin_url', null);
        $this->seeOther($origin_uri);
        return $this->text('This should never be reached.');
    }

    public function getLogContext(string $realm, string $username, ServerRequestInterface $server_request): array
    {
        $log_context = [
            'realm' => $realm,
            'username' => $username,
            'request_headers' => $server_request->getHeaders(),
            'server_params' => [],
        ];
        $server_params = $server_request->getServerParams();
        if (isset($server_params['REMOTE_ADDR'])) {
            $log_context['server_params']['REMOTE_ADDR'] = (string) $server_params['REMOTE_ADDR'];
        }
        if (isset($server_params['REMOTE_HOST'])) {
            $log_context['server_params']['REMOTE_HOST'] = (string) $server_params['REMOTE_HOST'];
        }
        if (isset($server_params['HTTP_X_FORWARDED_FOR'])) {
            $log_context['server_params']['HTTP_X_FORWARDED_FOR'] = (string) $server_params['HTTP_X_FORWARDED_FOR'];
        }
        if (isset($server_params['HTTP_CLIENT_IP'])) {
            $log_context['server_params']['HTTP_CLIENT_IP'] = (string) $server_params['HTTP_CLIENT_IP'];
        }
        return $log_context;
    }

}
