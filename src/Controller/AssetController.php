<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Controller;

use Exception;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Vitya\Component\Asset\AssetManager;

class AssetController extends AbstractController
{
    public function asset(
        string $pool,
        string $etag,
        string $file,
        AssetManager $asset_manager,
        ResponseFactoryInterface $response_factory,
        StreamFactoryInterface $stream_factory
    ): ResponseInterface {
        $asset_pool = $asset_manager->getAssetPool($pool);
        // Check URL validity.
        $current_etag = $asset_pool->getEtag();
        $expected_etag_slug = $current_etag;
        if ('' === $expected_etag_slug) {
            $expected_etag_slug = '-';
        }
        if ($expected_etag_slug !== $etag) {
            $this->notFound('URL does not match the current etag ("' . $etag . '" vs. "' . $expected_etag_slug . '").');
        }
        // Create the response.
        $response = $response_factory
            ->createResponse()
            ->withHeader('Cache-Control', 'public')
        ;
        if ($current_etag !== '') {
            $response = $response
                ->withHeader('ETag', $current_etag)
            ;
        }
        if (false === $this->isModified($this->getRequest(), $response)) {
            return $this->makeNotModifiedResponse($response);
        }
        // Check asset validity.
        if (false === $asset_path = realpath($asset_pool->getPath() . $file)) {
            $this->notFound('Non existing asset path (' . $asset_pool->getPath() . $file . ').');
        }
        if (false === $pool_path = realpath($asset_pool->getPath())) {
            $this->notFound('Non existing pool path (' . $asset_pool->getPath() . ').');
        }
        if (strpos($asset_path, $pool_path) !== 0) {
            $this->notFound('Invalid asset path (' . $asset_path . ').');
        }
        if (!is_file($asset_path)) {
            $this->notFound('Not a file (' . $asset_path . ').');
        }
        // Determine MIME type.
        $mime_type = 'application/octet-stream';
        $mime_types = [];
        $mime_types_lines = explode("\n", file_get_contents(__DIR__ . '/mime-types.csv'));
        foreach ($mime_types_lines as $mime_types_line) {
            if ($mime_types_line == '') {
                continue;
            }
            list($extensions_list, $type) = explode(',', $mime_types_line);
            $extensions = explode(' ', $extensions_list);
            foreach ($extensions as $extension) {
                $mime_types[$extension] = $type;
            }
        }
        $asset_filename_elements = explode('.', $asset_path);
        $asset_extension = mb_strtolower($asset_filename_elements[count($asset_filename_elements) - 1]);
        if (isset($mime_types[$asset_extension])) {
            $mime_type = $mime_types[$asset_extension];
        }
        $response = $response->withHeader('Content-Type', $mime_type);
        // Handle cache expiration.
        if ($asset_pool->getMaxAge() > 0) {
            $response = $response
                ->withAddedHeader('Cache-Control', 'max-age=' . $asset_pool->getMaxAge())
                ->withAddedHeader('Cache-Control', 's-max-age=' . $asset_pool->getMaxAge())
            ;
        }
        // Handle compression.
        if (in_array($mime_type, $asset_pool->getGzip())) {
            if (false === $content = file_get_contents($asset_path)) {
                throw new Exception('Could not read the asset file (' . $asset_path . ').');
            }
            if (false === $gzipped_content = gzencode($content)) {
                throw new Exception('Could not gzip the asset file (' . $asset_path . ').');
            }
            $response = $response
                ->withHeader('Content-Encoding', 'gzip')
                ->withBody($stream_factory->createStream($gzipped_content))
            ;
        } else {
            $response = $response->withBody($stream_factory->createStreamFromFile($asset_path));
        }
        // Done!
        return $response;
    }

}
