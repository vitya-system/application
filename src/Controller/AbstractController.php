<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Controller;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Connection;
use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Symfony\Component\Mailer\MailerInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Controller\Exception\ForbiddenException;
use Vitya\Component\Controller\Exception\NotFoundException;
use Vitya\Component\Controller\Exception\PermanentRedirectException;
use Vitya\Component\Controller\Exception\SeeOtherException;
use Vitya\Component\Controller\Exception\TemporaryRedirectException;
use Vitya\Component\Db\DoctrineCollection;
use Vitya\Component\Service\DependencyInjectorInterface;
use Vitya\Component\Session\HtmlFlashMessage;
use Vitya\Component\Session\FlashMessage;
use Vitya\Component\Session\FlashMessageInterface;
use Vitya\Component\Session\Session;

abstract class AbstractController
{
    private $applicationParameters = [];
    private $dependencyInjector = null;
    private $request = null;

    public function __construct(DependencyInjectorInterface $dependency_injector, RequestInterface $request, array $app_params)
    {
        $this->applicationParameters = $app_params;
        $this->dependencyInjector = $dependency_injector;
        $this->request = $request;
    }

    protected function getServiceFromHint(string $hint): object
    {
        $service = $this->dependencyInjector->getServiceFromHint($hint);
        if ($service === null) {
            throw new InvalidArgumentException('Could not locate a service for ' . $hint . '.');
        }
        return $service;
    }

    protected function getAppParam(string $name, $default_value = null)
    {
        if (!isset($this->applicationParameters[$name])) {
            return $default_value;
        }
        return $this->applicationParameters[$name];
    }

    // Http related methods.

    protected function seeOther(UriInterface $uri, string $message = ''): void
    {
        throw new SeeOtherException($uri, $message);
    }

    protected function temporaryRedirect(UriInterface $uri, string $message = ''): void
    {
        throw new TemporaryRedirectException($uri, $message);
    }

    protected function permanentRedirect(UriInterface $uri, string $message = ''): void
    {
        throw new PermanentRedirectException($uri, $message);
    }

    protected function forbidden(string $message = ''): void
    {
        throw new ForbiddenException($message);
    }

    protected function notFound(string $message = ''): void
    {
        throw new NotFoundException($message);
    }

    protected function isModified(RequestInterface $request, ResponseInterface $response): bool
    {
        $response_etag = $response->getHeaderLine('ETag');
        if ($response_etag !== '' && $request->getHeaderLine('If-None-Match') === $response_etag) {
            return false;
        }
        $if_modified_since = $request->getHeaderLine('If-Modified-Since');
        $if_modified_since_dt = DateTime::createFromFormat(DateTimeInterface::RFC7231, $if_modified_since);
        $last_modified = $response->getHeaderLine('Last-Modified');
        $last_modified_dt = DateTime::createFromFormat(DateTimeInterface::RFC7231, $last_modified);
        if ($if_modified_since_dt !== false && $last_modified_dt !== false) {
            if ($last_modified_dt <= $if_modified_since_dt) {
                return false;
            }
        }
        return true;
    }

    protected function makeNotModifiedResponse(ResponseInterface $response): ResponseInterface
    {
        $stream_factory = $this->getServiceFromHint('Psr\Http\Message\StreamFactoryInterface');
        $stream = $stream_factory->createStream('');
        $response = $response->withStatus(304)->withBody($stream);
        foreach (['Allow', 'Content-Encoding', 'Content-Language', 'Content-Length', 'Content-MD5', 'Content-Type', 'Last-Modified'] as $header) {
            $response = $response->withoutHeader($header);
        }
        return $response;
    }

    // Router related methods.

    protected function uri(string $route_name, array $parameters = null, array $query_parameters = null): UriInterface
    {
        $router = $this->getServiceFromHint('Vitya\Component\Route\RouterInterface');
        return $router->createUri($route_name, $parameters, $query_parameters);
    }

    protected function controllerUri(string $callable, array $parameters): UriInterface
    {
        $query_params = [
            'callable' => $callable,
            'parameters' => $parameters,
        ];
        $uri_factory = $this->getServiceFromHint('Psr\Http\Message\UriFactoryInterface');
        return $uri_factory->createUri('/_controller?' . http_build_query($query_params));
    }

    protected function makeAbsoluteUri(UriInterface $uri): UriInterface
    {
        $web_frontend = $this->getServiceFromHint('Vitya\Component\Frontend\WebFrontend');
        $main_server_request = $web_frontend->getMainServerRequest();
        if (null !== $main_server_request) {
            $context_uri = $main_server_request->getUri();
            $uri = $uri
                ->withScheme($context_uri->getScheme())
                ->withHost($context_uri->getHost())
                ->withUserInfo($context_uri->getUserInfo())
                ->withPort($context_uri->getPort())
            ;
        }
        return $uri;
    }

    // Session related methods.

    protected function getSession(): Session
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        return $session_service->getSession();
    }

    protected function addFlashMessage(string $queue_name, string $message): AbstractController
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        $session = $session_service->getSession();
        $session->getFlashMessageQueue($queue_name)->push(new FlashMessage($message));
        return $this;
    }

    protected function addHtmlFlashMessage(string $queue_name, string $html_title, string $html_body = ''): AbstractController
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        $session = $session_service->getSession();
        $session->getFlashMessageQueue($queue_name)->push(new HtmlFlashMessage($html_title, $html_body));
        return $this;
    }

    protected function getFlashMessage(string $queue_name): ?FlashMessageInterface
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        $session = $session_service->getSession();
        return $session->getFlashMessageQueue($queue_name)->pop();
    }

    protected function getAllFlashMessages(string $queue_name): array
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        $session = $session_service->getSession();
        return $session->getFlashMessageQueue($queue_name)->popAll();
    }

    // Twig and response related methods.

    protected function renderToString(string $template, array $parameters = []): string
    {
        $twig = $this->getServiceFromHint('Twig\Environment');
        $parameters = ['request' => $this->request] + $parameters;
        $result = $twig->render($template, $parameters);
        return $result;
    }

    protected function render(string $template, array $parameters = [], ResponseInterface $response = null): ResponseInterface
    {
        if ($response === null) {
            $response_factory = $this->getServiceFromHint('Psr\Http\Message\ResponseFactoryInterface');
            $response = $response_factory->createResponse();
        }
        $result = $this->renderToString($template, $parameters);
        $stream_factory = $this->getServiceFromHint('Psr\Http\Message\StreamFactoryInterface');
        $stream = $stream_factory->createStream($result);
        return $response->withBody($stream);
    }

    protected function outputString(string $s, ResponseInterface $response = null): ResponseInterface
    {
        if ($response === null) {
            $response_factory = $this->getServiceFromHint('Psr\Http\Message\ResponseFactoryInterface');
            $response = $response_factory->createResponse();
        }
        $stream_factory = $this->getServiceFromHint('Psr\Http\Message\StreamFactoryInterface');
        $stream = $stream_factory->createStream($s);
        return $response->withBody($stream);
    }

    protected function text(string $s, ResponseInterface $response = null): ResponseInterface
    {
        if ($response === null) {
            $response_factory = $this->getServiceFromHint('Psr\Http\Message\ResponseFactoryInterface');
            $response = $response_factory->createResponse();
        }
        $stream_factory = $this->getServiceFromHint('Psr\Http\Message\StreamFactoryInterface');
        $stream = $stream_factory->createStream($s);
        return $response->withHeader('Content-Type', 'text/plain; charset=utf-8')->withBody($stream);
    }

    protected function html(string $s, ResponseInterface $response = null): ResponseInterface
    {
        if ($response === null) {
            $response_factory = $this->getServiceFromHint('Psr\Http\Message\ResponseFactoryInterface');
            $response = $response_factory->createResponse();
        }
        $stream_factory = $this->getServiceFromHint('Psr\Http\Message\StreamFactoryInterface');
        $stream = $stream_factory->createStream($s);
        return $response->withHeader('Content-Type', 'text/html; charset=utf-8')->withBody($stream);
    }

    protected function json($value, ResponseInterface $response = null, int $flags = 0, int $depth = 512): ResponseInterface
    {
        if ($response === null) {
            $response_factory = $this->getServiceFromHint('Psr\Http\Message\ResponseFactoryInterface');
            $response = $response_factory->createResponse();
        }
        $stream_factory = $this->getServiceFromHint('Psr\Http\Message\StreamFactoryInterface');
        $stream = $stream_factory->createStream(json_encode($value, $flags, $depth));
        return $response->withHeader('Content-Type', 'application/json')->withBody($stream);
    }

    // Request related methods.

    protected function getMainRequest(): ?ServerRequestInterface
    {
        $web_frontend = $this->getServiceFromHint('Vitya\Component\Frontend\WebFrontend');
        return $web_frontend->getMainServerRequest();
    }

    protected function getMainRequestQueryParam(string $name, string $default_value = null): ?string
    {
        if (($request = $this->getMainRequest()) === null) {
            return $default_value;
        }
        $params = $request->getQueryParams();
        if (!isset($params[$name])) {
            return $default_value;
        }
        return $params[$name];
    }

    protected function getMainRequestBodyParam(string $name, string $default_value = null): ?string
    {
        if (($request = $this->getMainRequest()) === null) {
            return $default_value;
        }
        $params = $request->getParsedBody();
        if (!isset($params[$name])) {
            return $default_value;
        }
        return $params[$name];
    }

    protected function getMainRequestCookieParam(string $name, string $default_value = null): ?string
    {
        if (($request = $this->getMainRequest()) === null) {
            return $default_value;
        }
        $params = $request->getCookieParams();
        if (!isset($params[$name])) {
            return $default_value;
        }
        return $params[$name];
    }

    protected function getMainRequestServerParam(string $name, string $default_value = null): ?string
    {
        if (($request = $this->getMainRequest()) === null) {
            return $default_value;
        }
        $params = $request->getServerParams();
        if (!isset($params[$name])) {
            return $default_value;
        }
        return $params[$name];
    }

    protected function getRequest(): RequestInterface
    {
        return $this->request;
    }

    protected function getRequestQueryParam(string $name, string $default_value = null): ?string
    {
        $query = $this->request->getUri()->getQuery();
        parse_str($query, $params);
        if (!isset($params[$name])) {
            return $default_value;
        }
        return $params[$name];
    }

    // Db related methods.

    protected function dbs(): DoctrineCollection
    {
        return $this->getServiceFromHint('Vitya\Component\Db\DoctrineCollection');
    }

    protected function db(string $name = null): Connection
    {
        $pdo_collection = $this->getServiceFromHint('Vitya\Component\Db\DoctrineCollection');
        return $pdo_collection->getConnection($name);
    }

    // Authentication related methods.

    protected function requireAuthentication(string $realm_name): void
    {
        $authentication_service = $this->getServiceFromHint('Vitya\Component\Authentication\AuthenticationService');
        $authentication_service->authenticate($realm_name, $this->getMainRequest());
        if ($authentication_service->getUser($realm_name) === null) {
            $this->forbidden();
        }
    }

    protected function getUser(string $realm_name): ?UserInterface
    {
        $authentication_service = $this->getServiceFromHint('Vitya\Component\Authentication\AuthenticationService');
        return $authentication_service->getUser($realm_name);
    }

    protected function logout(): void
    {
        $authentication_service = $this->getServiceFromHint('Vitya\Component\Authentication\AuthenticationService');
        $authentication_service->logout();
    }

    // Mailer related methods.

    protected function mailer(): ?MailerInterface
    {
        $mailer = $this->getServiceFromHint('Symfony\Component\Mailer\MailerInterface');
        return $mailer;
    }

    // Widget factory related methods.

    protected function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

}
