<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

namespace Vitya\Application\Helper;

use Exception;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class PaginationHelper
{
    private $request = null;
    private $currentPage = 1;
    private $nbItems = 0;
    private $possibleNbsItemsPerPage = [10];
    private $nbItemsPerPage = 10;
    private $prefix = '';

    public function __construct(ServerRequestInterface $request = null, string $prefix = '')
    {
        $this->request = $request;
        $this->prefix = $prefix;
        if (null !== $this->request) {
            $params = $request->getQueryParams();
            if (isset($params[$prefix . 'page'])) {
                $this->setCurrentPage((int) $params[$prefix . 'page']);
            }
            if (isset($params[$prefix . 'items'])) {
                $this->setNbItemsPerPage((int) $params[$prefix . 'items']);
            }
        }
    }

    public function getRequest(): ?ServerRequestInterface
    {
        return $this->request;
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function setCurrentPage(int $n): PaginationHelper
    {
        $this->currentPage = $n;
        return $this;
    }

    public function getNbItems(): int
    {
        return $this->nbItems;
    }

    public function setNbItems(int $n): PaginationHelper
    {
        $this->nbItems = $n;
        return $this;
    }

    public function getPossibleNbsItemsPerPage(): array
    {
        return $this->possibleNbsItemsPerPage;
    }

    public function setPossibleNbsItemsPerPage(array $possible_nbs_items_per_page): PaginationHelper
    {
        $this->possibleNbsItemsPerPage = [];
        foreach ($possible_nbs_items_per_page as $possible_nb_items_per_page) {
            if (is_int($possible_nb_items_per_page) && 0 < $possible_nb_items_per_page) {
                $this->possibleNbsItemsPerPage[] = $possible_nb_items_per_page;
            }
        }
        if (0 === count($this->possibleNbsItemsPerPage)) {
            $this->possibleNbsItemsPerPage = [10];
        }
        return $this;
    }

    public function getNbItemsPerPage(): int
    {
        return $this->nbItemsPerPage;
    }

    public function setNbItemsPerPage(int $n): PaginationHelper
    {
        $this->nbItemsPerPage = $n;
        return $this;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getNbPages(): int
    {
        $this->assertValidConfiguration();
        $nb_pages = (int) ceil($this->nbItems / $this->nbItemsPerPage);
        return $nb_pages;
    }

    public function getFirstItemOffset(): int
    {
        $this->assertValidConfiguration();
        $first_item_offset = ($this->currentPage - 1) * $this->nbItemsPerPage;
        return $first_item_offset;
    }

    public function getLastItemOffset(): int
    {
        $this->assertValidConfiguration();
        $last_item_offset = min($this->currentPage * $this->nbItemsPerPage - 1, $this->nbItems - 1);
        return $last_item_offset;
    }

    public function getDecoratedUri(UriInterface $uri, int $page, int $nb_items_per_page)
    {
        $this->assertValidConfiguration();
        parse_str($uri->getQuery(), $query_params);
        $query_params[$this->prefix . 'page'] = (string) $page;
        if (1 === $page) {
            unset($query_params[$this->prefix . 'page']);
        }
        $query_params[$this->prefix . 'items'] = (string) $nb_items_per_page;
        if ($this->possibleNbsItemsPerPage[0] === $nb_items_per_page) {
            unset($query_params[$this->prefix . 'items']);
        }
        $uri = $uri->withQuery(http_build_query($query_params));
        return $uri;
    }

    public function assertValidConfiguration(): void
    {
        if (0 > $this->nbItems) {
            throw new Exception('Number of items cannot be negative.');
        }
        if (0 >= $this->nbItemsPerPage) {
            throw new Exception('Number of items per page cannot be negative or equal to zero.');
        }
        if (0 >= $this->currentPage) {
            throw new Exception('Page number cannot be negative or equal to zero.');
        }
        if (false === in_array($this->nbItemsPerPage, $this->possibleNbsItemsPerPage)) {
            throw new Exception('This number of items per page is not allowed.');
        }
    }

}
