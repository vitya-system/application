<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\Twig;

use InvalidArgumentException;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFunction;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Service\DependencyInjectorInterface;
use Vitya\Component\Session\FlashMessageInterface;
use Vitya\Component\Widget\WidgetInterface;

class ApplicationTwigExtension extends AbstractExtension implements GlobalsInterface
{
    private $applicationParameters = [];
    private $dependencyInjector = null;
    private $requiredCss = [];
    private $requiredJs = [];

    public function __construct(
        array $app_params,
        DependencyInjectorInterface $dependency_injector
    ) {
        $this->applicationParameters = $app_params;
        $this->dependencyInjector = $dependency_injector;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('url', [$this, 'url']),
            new TwigFunction('absolute_url', [$this, 'absoluteUrl']),
            new TwigFunction('render_controller', [$this, 'renderController'], ['is_safe' => ['html']]),
            new TwigFunction('render_widget', [$this, 'renderWidget'], ['is_safe' => ['html']]),
            new TwigFunction('get_flash_message', [$this, 'getFlashMessage']),
            new TwigFunction('get_all_flash_messages', [$this, 'getAllFlashMessages']),
            new TwigFunction('user', [$this, 'user']),
            new TwigFunction('asset', [$this, 'asset']),
            new TwigFunction('require_css', [$this, 'requireCss']),
            new TwigFunction('get_required_css', [$this, 'getRequiredCss'], ['is_safe' => ['html']]),
            new TwigFunction('require_js', [$this, 'requireJs']),
            new TwigFunction('get_required_js', [$this, 'getRequiredJs'], ['is_safe' => ['html']]),
        ];
    }

    public function getGlobals(): array
    {
        return [
            'app_params' => $this->applicationParameters,
        ];
    }

    private function getServiceFromHint(string $hint): object
    {
        $service = $this->dependencyInjector->getServiceFromHint($hint);
        if ($service === null) {
            throw new InvalidArgumentException('Could not locate a service for ' . $hint . '.');
        }
        return $service;
    }

    public function url(string $route_name, array $parameters = null, array $query_parameters = null): string
    {
        $router = $this->getServiceFromHint('Vitya\Component\Route\RouterInterface');
        return (string) $router->createUri($route_name, $parameters, $query_parameters);
    }

    public function absoluteUrl(string $route_name, array $parameters = null, array $query_parameters = null): string
    {
        $router = $this->getServiceFromHint('Vitya\Component\Route\RouterInterface');
        $web_frontend = $this->getServiceFromHint('Vitya\Component\Frontend\WebFrontend');
        $uri = $router->createUri($route_name, $parameters, $query_parameters);
        $main_server_request = $web_frontend->getMainServerRequest();
        if (null !== $main_server_request) {
            $context_uri = $main_server_request->getUri();
            $uri = $uri
                ->withScheme($context_uri->getScheme())
                ->withHost($context_uri->getHost())
                ->withUserInfo($context_uri->getUserInfo())
                ->withPort($context_uri->getPort())
            ;
        }
        return (string) $uri;
    }

    public function renderController(string $callable, array $parameters = []): string
    {
        $uri_factory = $this->getServiceFromHint('Psr\Http\Message\UriFactoryInterface');
        $request_factory = $this->getServiceFromHint('Psr\Http\Message\RequestFactoryInterface');
        $machine = $this->getServiceFromHint('Vitya\Component\Machine\MachineInterface');
        $query_params = [
            'callable' => $callable,
            'parameters' => $parameters,
        ];
        $uri = $uri_factory->createUri('/_controller?' . http_build_query($query_params));
        $request = $request_factory->createRequest('GET', $uri);
        return (string) $machine->handle($request)->getBody();
    }

    public function renderWidget(WidgetInterface $widget): string
    {
        return $widget->render();
    }

    public function getFlashMessage(string $queue_name): ?FlashMessageInterface
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        $session = $session_service->getSession();
        return $session->getFlashMessageQueue($queue_name)->pop();
    }

    public function getAllFlashMessages(string $queue_name): array
    {
        $session_service = $this->getServiceFromHint('Vitya\Component\Session\SessionService');
        $session = $session_service->getSession();
        return $session->getFlashMessageQueue($queue_name)->popAll();
    }

    public function user(string $realm_name): ?UserInterface
    {
        $authentication_service = $this->getServiceFromHint('Vitya\Component\Authentication\AuthenticationService');
        return $authentication_service->getUser($realm_name);
    }

    public function asset(string $pool, string $file): string
    {
        $asset_manager = $this->getServiceFromHint('Vitya\Component\Asset\AssetManager');
        $asset_pool = $asset_manager->getAssetPool($pool);
        $etag_slug = $asset_pool->getEtag();
        if ($etag_slug == '') {
            $etag_slug = '-';
        }
        return $this->absoluteUrl('asset', ['pool' => $pool, 'etag' => $etag_slug, 'file' => $file]);
    }

    public function requireCss(string $context, string $url, array $attributes = []): void
    {
        if (false === isset($this->requiredCss[$context])) {
            $this->requiredCss[$context] = [];
        }
        $this->requiredCss[$context][$url] = [
            'url' => $url,
            'attributes' => $attributes,
        ];
    }

    public function getRequiredCss(string $context): string
    {
        $s = '';
        if (false === isset($this->requiredCss[$context])) {
            return '';
        }
        foreach ($this->requiredCss[$context] as $required_css) {
            $required_css['attributes'] = ['rel' => 'stylesheet', 'type' => 'text/css'] + $required_css['attributes'];
            $tag = '<link href="' . htmlspecialchars($required_css['url']) . '"';
            foreach ($required_css['attributes'] as $k => $v) {
                if (is_string($v)) {
                    $tag .= ' ' . htmlspecialchars($k) . '="' . htmlspecialchars($v) . '"';
                } else {
                    $tag .= ' ' . htmlspecialchars($k);
                }
            }
            $tag .= '>';
            if ('' !== $s) {
                $s .= "\n";
            }
            $s .= $tag;
        }
        return $s;
    }

    public function requireJs(string $context, string $url, array $attributes = []): void
    {
        if (false === isset($this->requiredJs[$context])) {
            $this->requiredJs[$context] = [];
        }
        $this->requiredJs[$context][$url] = [
            'url' => $url,
            'attributes' => $attributes,
        ];
    }

    public function getRequiredJs(string $context): string
    {
        $s = '';
        if (false === isset($this->requiredJs[$context])) {
            return '';
        }
        foreach ($this->requiredJs[$context] as $required_js) {
            $tag = '<script src="' . htmlspecialchars($required_js['url']) . '"';
            foreach ($required_js['attributes'] as $k => $v) {
                if (is_string($v)) {
                    $tag .= ' ' . htmlspecialchars($k) . '="' . htmlspecialchars($v) . '"';
                } else {
                    $tag .= ' ' . htmlspecialchars($k);
                }
            }
            $tag .= '></script>';
            if ('' !== $s) {
                $s .= "\n";
            }
            $s .= $tag;
        }
        return $s;
    }

}
