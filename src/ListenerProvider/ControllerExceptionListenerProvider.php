<?php

/*
 * Copyright 2020, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ListenerProvider;

use Exception;
use Psr\EventDispatcher\ListenerProviderInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Throwable;
use Vitya\Component\Http\Response;
use Vitya\Component\Controller\Event\ControllerExceptionEvent;
use Vitya\Component\Controller\Exception\ForbiddenException;
use Vitya\Component\Controller\Exception\NotFoundException;
use Vitya\Component\Controller\Exception\PermanentRedirectException;
use Vitya\Component\Controller\Exception\SeeOtherException;
use Vitya\Component\Controller\Exception\TemporaryRedirectException;
use Vitya\Component\Log\LoggerCollectionInterface;

class ControllerExceptionListenerProvider implements ListenerProviderInterface
{
    private $debug = false;
    private $loggerCollection = null;
    private $responseFactory = null;
    private $streamFactory = null;

    public function __construct(
        ResponseFactoryInterface $response_factory,
        StreamFactoryInterface $stream_factory,
        LoggerCollectionInterface $logger_collection,
        bool $debug
    ) {
        $this->debug = $debug;
        $this->responseFactory = $response_factory;
        $this->streamFactory = $stream_factory;
        $this->loggerCollection = $logger_collection;
    }

    public function getListenersForEvent(object $event): iterable
    {
        $listeners = [];
        if ($event instanceof ControllerExceptionEvent) {
            $listeners[] = [$this, 'createErrorPageResponse'];
        }
        return $listeners;
    }

    public function getResponseFactory()
    {
        return $this->responseFactory;
    }

    public function getStreamFactory()
    {
        return $this->streamFactory;
    }

    public function getLoggerCollection(): LoggerCollectionInterface
    {
        return $this->loggerCollection;
    }

    public function createErrorPageResponse(object $event): void
    {
        if (false === $event instanceof ControllerExceptionEvent) {
            throw new Exception(''
                . 'The event passed to this listener must be an instance of '
                . 'Vitya\Component\Controller\Event\ControllerExceptionEvent.'
            );
        }
        $exception = $event->getThrowable();
        $code = 0;
        $headers = null;
        $html = null;
        if ($exception instanceof SeeOtherException) {
            $code = 303;
            $headers = ['Location' => (string) $exception->getUri()];
        } elseif ($exception instanceof TemporaryRedirectException) {
            $code = 307;
            $headers = ['Location' => (string) $exception->getUri()];
        } elseif ($exception instanceof PermanentRedirectException) {
            $code = 308;
            $headers = ['Location' => (string) $exception->getUri()];
        } elseif ($exception instanceof ForbiddenException) {
            $code = 403;
            $html = $this->render($code, $exception);
        } elseif ($exception instanceof NotFoundException) {
            $code = 404;
            $html = $this->render($code, $exception);
        } else {
            $code = 500;
            $logger = $this->loggerCollection->getLogger('app');
            $log_context = [
                'exception_class' => get_class($exception),
                'exception_message' => $exception->getMessage(),
                'stack_trace' => $exception->getTraceAsString(),
            ];
            $logger->critical('Exception thrown in ' . $exception->getFile() . ' at line ' . $exception->getLine() . '.', $log_context);
            $html = $this->render($code, $exception);
        }
        $response = $this->responseFactory->createResponse($code);
        if ($headers !== null) {
            foreach ($headers as $k => $header) {
                $response = $response->withAddedHeader($k, $header);
            }
        }
        if ($html !== null) {
            $body = $this->streamFactory->createStream($html);
            $response = $response->withBody($body);
        }
        $event->setResponse($response);
    }

    private function render(int $code, Throwable $throwable): string
    {
        $reason = '?';
        if (isset(Response::DEFAULT_REASON_PHRASES[$code])) {
            $reason = Response::DEFAULT_REASON_PHRASES[$code];
        }
        ob_start();
        if (false === $this->debug) {
            include __DIR__ . '/../../views/ListenerProvider/error.inc.php';
        } else {
            include __DIR__ . '/../../views/ListenerProvider/error_debug.inc.php';
        }
        return ob_get_clean();
    }

}
