<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ListenerProvider;

use Psr\EventDispatcher\ListenerProviderInterface;
use Throwable;
use TypeError;
use Vitya\Component\Authentication\Exception\AuthenticationProcedureException;
use Vitya\Component\Http\Response;
use Vitya\Component\Controller\Event\ControllerExceptionEvent;

class AuthenticationExceptionListenerProvider implements ListenerProviderInterface
{
    public function getListenersForEvent(object $event): iterable
    {
        $listeners = [];
        if ($event instanceof ControllerExceptionEvent) {
            $listeners[] = [$this, 'setEventResponse'];
        }
        return $listeners;
    }

    public function setEventResponse(object $event): void
    {
        if (!$event instanceof ControllerExceptionEvent) {
            throw new TypeError(''
                . 'The event passed to this listener must be an instance of '
                . 'Vitya\Component\Controller\Event\ControllerExceptionEvent.'
            );
        }
        $exception = $event->getThrowable();
        if ($exception instanceof AuthenticationProcedureException) {
            $event->setResponse($exception->getResponse());
        }
    }

}
