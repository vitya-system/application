<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use Vitya\Component\Http\HttpFactory;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class HttpFactoryServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'http_factory';
    }

    public function getTypeHints(): array
    {
        return [
            'Psr\Http\Message\RequestFactoryInterface',
            'Psr\Http\Message\ResponseFactoryInterface',
            'Psr\Http\Message\ServerRequestFactoryInterface',
            'Psr\Http\Message\StreamFactoryInterface',
            'Psr\Http\Message\UploadedFileFactoryInterface',
            'Psr\Http\Message\UriFactoryInterface',
            'Vitya\Component\Http\CookieFactoryInterface',
            'Vitya\Component\Http\HttpFactoryInterface',
        ];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        return new HttpFactory();
    }

}
