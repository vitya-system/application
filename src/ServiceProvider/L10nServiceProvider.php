<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use InvalidArgumentException;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;
use Vitya\Component\L10n\Locale;
use Vitya\Component\L10n\LocaleCatalog;

class L10nServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'l10n';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\L10n\LocaleCatalog'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        $locale_catalog = new LocaleCatalog();
        foreach ($service_container['l10n__locales'] as $locale_id => $locale_definition) {
            if (false === is_string($locale_id)) {
                throw new InvalidArgumentException(
                    'Locale identifier must be a string, ' . get_type($locale_id). ' given instead.'
                );
            }
            if (false === isset($locale_definition['name'])) {
                throw new InvalidArgumentException(
                    'Locale definition must contain a name (' . $locale_id. ').'
                );
            }
            $locale = new Locale($locale_id, $locale_definition['name']);
            $locale_catalog->addLocale($locale);
        }
        if ($service_container['l10n__default_locale'] !== '') {
            $locale_catalog->setDefaultLocaleId($service_container['l10n__default_locale']);
        }
        return $locale_catalog;
    }

}
