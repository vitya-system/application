<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use Vitya\Component\Route\Route;
use Vitya\Component\Route\RouteCollection;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class RouteCollectionServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'route_collection';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\Route\RouteCollection'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        $route_collection = new RouteCollection(
            $service_container['base_url_path'],
            $service_container->get('http_factory')
        );
        if (isset($service_container['routes']) && is_array($service_container['routes'])) {
            foreach ($service_container['routes'] as $route_name => $route_definition) {
                $route = new Route($service_container->get('http_factory'));
                $route
                    ->setFromArray($route_name, $route_definition)
                    ->compile()
                ;
                $route_collection->setRoute($route);
            }
        }
        return $route_collection;
    }

}
