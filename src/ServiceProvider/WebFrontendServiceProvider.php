<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use Vitya\Component\Frontend\WebFrontend;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class WebFrontendServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'web_frontend';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\Frontend\WebFrontend'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        return new WebFrontend(
            $service_container->get('request_handler'),
            $service_container->get('http_factory'),
            $service_container->get('http_factory'),
            $service_container->get('http_factory'),
            $service_container->get('http_factory'),
            $service_container->get('http_factory'),
            $service_container['web_frontend__forced_domain_name']
        );
    }

}
