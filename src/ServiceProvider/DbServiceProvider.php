<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use Vitya\Component\Db\DoctrineCollection;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class DbServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'db';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\Db\DoctrineCollection'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        $doctrine_collection = new DoctrineCollection();
        foreach ($service_container['db'] as $db_key => $db_config) {
            $base_db_config = [
                'url' => '',
            ];
            $db_config = $db_config + $base_db_config;
            $doctrine_collection->add(
                $db_key,
                $db_config['url'],
            );
        }
        return $doctrine_collection;
    }

}
