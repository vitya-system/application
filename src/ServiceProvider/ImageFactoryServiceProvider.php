<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use Vitya\Component\Image\ImageFactory;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class ImageFactoryServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'image';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\Image\ImageFactoryInterface'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        $image_factory = new ImageFactory($service_container['image__backend'], $service_container->get('http_factory'));
        return $image_factory;
    }

}
