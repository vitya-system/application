<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;
use Vitya\Component\Asset\AssetManager;

class AssetManagerServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'assets';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\Asset\AssetManager'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        $asset_manager = new AssetManager();
        foreach ($service_container['assets'] as $name => $asset_pool_definition) {
            $base_asset_pool_definition = [
                'path' => __DIR__,
                'etag' => '',
                'etag_file' => '',
                'max_age' => 0,
                'gzip' => [],
            ];
            $asset_pool_definition = $asset_pool_definition + $base_asset_pool_definition;
            $asset_manager->addAssetPool(
                $name,
                $asset_pool_definition['path'],
                $asset_pool_definition['etag'],
                $asset_pool_definition['etag_file'], 
                $asset_pool_definition['max_age'],
                $asset_pool_definition['gzip']
            );
        }
        return $asset_manager;
    }

}
