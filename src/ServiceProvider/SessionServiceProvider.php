<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\Application\ServiceProvider;

use InvalidArgumentException;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;
use Vitya\Component\Session\SessionHandler;
use Vitya\Component\Session\SessionService;

class SessionServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'session';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\Component\Session\SessionService'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        if ($service_container['session__gc_probability'] < 0.0 || $service_container['session__gc_probability'] > 1.0) {
            throw new InvalidArgumentException('Session garbage collection probability must be comprised between 0 and 1.');
        }
        $handler = new SessionHandler(
            $service_container['sessions_dir'],
            $service_container['session__name']
        );
        return new SessionService(
            $handler,
            $service_container['session__name'],
            $service_container['session__lifetime'],
            $service_container['session__gc_probability'],
            $service_container['base_url_path']
        );
    }

}
