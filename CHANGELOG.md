# Changelog

## [1.0.0-alpha14] - 2025-03-07
### Chaged
- Transform cms-ui into cms-application.

## [1.0.0-alpha13] - 2025-02-21
### Added
- Add image manipulation functionalities.

## [1.0.0-alpha12] - 2024-09-30
### Added
- Logger.

## [1.0.0-alpha11] - 2024-07-16
### Added
- Pagination helper.

## [1.0.0-alpha10] - 2022-07-01
### Added
- Added a login page controller that works together with the new login page
  authenticator.
- Added an image processing service. 
### Changed
- The Twig service provider is optional again

## [1.0.0-alpha9] - 2022-06-03
### Changed
- Adapt to the big refactoring of the CMS part.
- Bumped version to alpha9 to match the rest of the system.

## [1.0.0-alpha8] - 2021-08-02
### Changed
- Bumped version to alpha8 to match the rest of the system.

## [1.0.0-alpha7] - 2021-07-05
### Removed
- Remove WidgetFactory service.
### Fixed
- Asset mime type detection is case-insensitive.

## [1.0.0-alpha6] - 2021-06-07
### Changed
- Reorganize router.

## [1.0.0-alpha5] - 2021-05-03
### Changed
- You can now chain calls to addFlashMessages().
- Saner default locale configuration.

## [1.0.0-alpha4] - 2021-04-07
### Changed
- Bumped version to alpha4 to match the rest of the system.

## [1.0.0-alpha3] - 2021-03-01
### Added
- Assets can now be served gzipped.
- Absolute URLs generation functions.
- The domain name can be forced using a systematic redirection.
- New l10n service.
- HSTS headers can be configured.

### Changed
- More reliable detection of asset MIME types.
- Better handling of assets cacheing.

## [1.0.0-alpha2] - 2021-02-01
### Added
- New method in AbstractController to get a WidgetFactory.
- New application parameter: environment.
- New helper functions in AbstractController to deal with unmodified responses.
## Changed
- Assets: generate an etag using the etag_file mtime rather than its content.
- Set debug mode to false by default
## Fixed
- The logout controller now takes base_url_path into account.
- Make sure the function args are included in stack traces with PHP >= 7.4
- Do not set a return type for AbstractController::getAppParam()

## [1.0.0-alpha1] - 2021-01-04
### Added
- First release.
