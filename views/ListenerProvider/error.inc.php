<?php

declare(strict_types=1);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo htmlspecialchars('Error ' . $code . ' – ' . $reason); ?></title>
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            html {
                font-size: 16px;
                line-height: 1.5;
                color: #ffffff;
                background-color: #343957;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
                height: 100%;
            }
            html, input {
                font-family: sans-serif;
            }
            body {
                text-align: center;
                height: 100%;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .container {
                margin-bottom: 30px;
            }
            h1 {
                font-size: 100px;
                font-weight: normal;
                line-height: 110px;
                margin: 0;
            }
            h2 {
                font-size: 20px;
                font-weight: normal;
                margin: 0;
            }
            h3 {
                font-size: 45px;
                margin: 20px 0 0;
            }
            @media only screen and (min-width: 640px) {
                h1 {
                    font-size: 200px;
                    line-height: 220px;
                }
                h2 {
                    font-size: 30px;
                }
                h3 {
                    font-size: 60px;
                }
            }
            @media only screen and (min-width: 1280px) {
                h1 {
                    font-size: 250px;
                    line-height: 270px;
                }
                h2 {
                    font-size: 40px;
                }
                h3 {
                    font-size: 80px;
                }
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1><?php echo $code; ?></h1>
            <h2><?php echo htmlspecialchars($reason); ?></h2>
            <h3>⚑</h3>
        </div>
    </body>
</html>
