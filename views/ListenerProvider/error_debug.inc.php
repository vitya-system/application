<?php

declare(strict_types=1);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo htmlspecialchars('Error ' . $code . ' – ' . $reason); ?></title>
        <style>
            * {
                margin: 0;
                padding: 0;
            }
            body {
                background-color: #e1e2e6;
                color: #343957;
                font-family: sans-serif;
            }
            .container {
                max-width: 60rem;
                margin: 1rem auto;
            }
            .header {
                display: flex;
                align-items: baseline;
                background-color: #343957;
                border-radius: 0.333333333rem 0.333333333rem 0 0;
            }
            .status_code {
                padding: 1rem;
                background-color: #292d45;
                color: #fff;
                font-size: 2rem;
                border-radius: 0.333333333rem 0 0 0;
            }
            .reason {
                padding: 1rem;
                background-color: #343957;
                color: #fff;
                font-size: 2rem;
                border-radius: 0 0.333333333rem 0 0;
            }
            .exception_message {
                padding: 1rem;
                background-color: #f5f5f6;
            }
            .exception_message_file_details {
                margin: 0.5rem 0 0 0;
                font-size: 0.75rem;
            }
            .stack_trace {
                background-color: #fff;
            }
            .trace_element {
                padding: 1rem;
                background-color: #fff;
            }
            .trace_element:nth-child(even) {
                background-color: #f5f5f6;
            }
            .trace_element_function {
                font-weight: bold;
            }
            .trace_element_args {
                color: #737373;
            }
            .trace_element_file_details {
                margin: 0.5rem 0 0 0;
                font-size: 0.75rem;
            }
            .footer {
                padding: 1rem;
                border-radius: 0 0 0.333333333rem 0.333333333rem;
                background-color: #343957;
                color: #fff;
                text-align: right;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <div class="status_code"><?php echo $code; ?></div>
                <div class="reason"><?php echo htmlspecialchars($reason); ?></div>
            </div>
            <div class="exception_message">
                <strong><?php echo htmlspecialchars(get_class($throwable)); ?></strong>
                <br>
                <?php echo htmlspecialchars($throwable->getMessage()); ?>
                <div class="exception_message_file_details">
                    <?php
                    $file_elements = explode('/', $throwable->getFile());
                    echo ''
                        . 'in <strong><abbr title="' . htmlspecialchars($throwable->getFile()) . '">'
                        . htmlspecialchars(array_pop($file_elements))
                        . '</abbr></strong>';
                    echo ' at line ' . $throwable->getLine();
                    ?>
                </div>
            </div>
            <div class="stack_trace">
                <?php
                foreach ($throwable->getTrace() as $k => $v) {
                    ?>
                    <div class="trace_element">
                        #<?php echo $k; ?>
                        <span class="trace_element_function">
                            <?php
                            if (isset($v['class']) && $v['class'] != '') {
                                $class_elements = explode('\\', $v['class']);
                                $short_class_name = array_pop($class_elements);
                                echo
                                    '<abbr title="' . htmlspecialchars($v['class'] . $v['type'] . $v['function']) . '">'
                                    . htmlspecialchars($short_class_name . $v['type'] . $v['function'])
                                    . '</abbr>';
                            } else {
                                echo htmlspecialchars($v['function']);
                            }
                            ?></span><span class="trace_element_args"><?php
                            $trace_element_args_array = [];
                            echo '(';
                            if (isset($v['args'])) {
                                foreach ($v['args'] as $k_arg => $arg) {
                                    $trace_element_arg_string = '';
                                    $trace_element_arg_string .= htmlspecialchars(gettype($arg));
                                    if (is_object($arg)) {
                                        $class_elements = explode('\\', get_class($arg));
                                        $short_class_name = array_pop($class_elements);
                                        $trace_element_arg_string .= ' '
                                            . '<abbr title="' . htmlspecialchars(get_class($arg)) . '">'
                                            . htmlspecialchars($short_class_name)
                                            . '</abbr>';
                                    } elseif (is_array($arg)) {
                                        $trace_element_arg_string .= ' [';
                                        $arg_items = [];
                                        foreach ($arg as $k_item => $v_item) {
                                            $arg_items[] = $k_item . ' => ' . (is_scalar($v_item) ? (string) $v_item : gettype($v_item));
                                        }
                                        $trace_element_arg_string .= htmlspecialchars(implode(', ', $arg_items));
                                        $trace_element_arg_string .= ']';
                                    } elseif (is_scalar($arg)) {
                                        $trace_element_arg_string .= ' ' . htmlspecialchars((string) $arg);
                                    }
                                    $trace_element_args_array[] = $trace_element_arg_string;
                                }
                                echo implode(', ', $trace_element_args_array);
                            }
                            echo ')';
                            ?>
                        </span>
                        <div class="trace_element_file_details">
                            <?php
                            if (isset($v['file'])) {
                                $file_elements = explode('/', $v['file']);
                                echo ' in <strong><abbr title="' . htmlspecialchars($v['file']) . '">' . htmlspecialchars(array_pop($file_elements)) . '</abbr></strong>';
                            }
                            ?>
                            <?php
                            if (isset($v['line'])) {
                                echo ' at line ' . $v['line'];
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="footer">
                Vitya
            </div>
        </div>
    </body>
</html>
